package utils;

public class PointPlanner {
	private float distance = 50;
	private float offX = 0;
	private float offY = 0;
	private int x;
	private int y;
	
	public PointPlanner(int x, int y) {
		this.x = x;
		this.y = y;		
	}
	
	public float getX() {
		return x*distance+offX;
	}
	
	public float getY() {
		return y*distance+offY;
	}
}
