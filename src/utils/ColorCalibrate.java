package utils;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.robotics.filter.MeanFilter;

public class ColorCalibrate {
	private ColorSensor colorSensor;
	private SampleProvider average; 
	private float[] sample;
	private float[] blue;
	private float[] red; 
	private float[] green;
	private float[] yellow;
	private float[] white;
	private float[] black;
	private float[] gray;
	
	public ColorCalibrate(ColorSensor cs) {
		this.colorSensor = cs;
		this.start();
		average = new MeanFilter(colorSensor.getRGBMode(), 1);
		sample = new float[average.sampleSize()];
		calibrate();
	}
	
	public void calibrate() {
		
	
		
		LCD.clear();
		LCD.drawString("Calibrate blue...", 0, 1);
		Button.ENTER.waitForPressAndRelease();
		this.blue = new float[average.sampleSize()];
		average.fetchSample(blue, 0);
		//LCD.drawString(""+this.blue, 0, 3);
		//Button.ENTER.waitForPressAndRelease();
		
		LCD.clear();
		LCD.drawString("Calibrate red...", 0, 1);
		Button.ENTER.waitForPressAndRelease();
		this.red = new float[average.sampleSize()];
		average.fetchSample(red, 0);

		
		LCD.clear();
		LCD.drawString("Calibrate green...", 0, 1);;
		Button.ENTER.waitForPressAndRelease();
		this.green = new float[average.sampleSize()];
		average.fetchSample(green, 0);

		
		LCD.clear();
		LCD.drawString("Calibrate yellow...", 0, 1);
		Button.ENTER.waitForPressAndRelease();
		this.yellow = new float[average.sampleSize()];
		average.fetchSample(yellow, 0);

		
		LCD.clear();
		LCD.drawString("Calibrate white...", 0, 1);		
		Button.ENTER.waitForPressAndRelease();
		this.white = new float[average.sampleSize()];
		average.fetchSample(white, 0);

		LCD.clear();
		LCD.drawString("Calibrate black...", 0, 1);		
		Button.ENTER.waitForPressAndRelease();
		this.black = new float[average.sampleSize()];
		average.fetchSample(black, 0);
		LCD.drawString(""+ this.black, 0, 3);

		LCD.clear();
		LCD.drawString("Calibrate gray...", 0, 1);
		Button.ENTER.waitForPressAndRelease();
		this.gray = new float[average.sampleSize()];
		average.fetchSample(gray, 0);

		LCD.drawString("Colors calibrated!", 0, 1);		
	}
	
	public float[] getRed() {
		return this.red;
	}
	
	public float[] getGray() {
		return this.gray;
	}
	public float[] getBlue() {
		return this.blue;
	}
	
	public float[] getYellow() {
		return this.yellow;
	}
	
	public float[] getGreen() {
		return this.green;
	}
	
	public float[] getWhite() {
		return this.white;
	}
	
	public float[] getBlack() {
		return this.black;
	}
	
	public int detectColor() {
		average.fetchSample(sample, 0);
		double minscal = Double.MAX_VALUE;
		int color = 0;
		
		double scalaire = scalaire(sample, blue);
		if (scalaire < minscal) {
			minscal = scalaire;
			color = 1;
		}
		
		scalaire = scalaire(sample, red);
		if (scalaire < minscal) {
			minscal = scalaire;
			color = 2;
		}
		
		scalaire = scalaire(sample, green);
		if (scalaire < minscal) {
			minscal = scalaire;
			color = 3;
		}
		
		scalaire = scalaire(sample, yellow);
		if (scalaire < minscal) {
			minscal = scalaire;
			color = 4;
		}
		
		scalaire = scalaire(sample, white);
		if (scalaire < minscal) {
			minscal = scalaire;
			color = 5;
		}
		
		scalaire = scalaire(sample, black);
		if (scalaire < minscal) {
			minscal = scalaire;
			color = 6;
		}
		
		scalaire = scalaire(sample, gray);
		if (scalaire < minscal) {
			minscal = scalaire;
			color = 7;
		}
		
		return color;
	}
	
	public void start() {
		this.colorSensor.setFloodlight(Color.WHITE);
	}
	
	public void close() {
		colorSensor.setFloodlight(false);
	}
	
	public double scalaire(float[] v1, float[] v2) {
		return Math.sqrt (Math.pow(v1[0] - v2[0], 2.0) +
				Math.pow(v1[1] - v2[1], 2.0) +
				Math.pow(v1[2] - v2[2], 2.0));
	}
	
}
