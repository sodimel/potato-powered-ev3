package utils;


import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3UltrasonicSensor;

public class Vision {

	private EV3UltrasonicSensor sonar   = null;
	private Port                port    = null;

	public Vision(){
		port  = LocalEV3.get().getPort(Const.DISTANCE_SENSOR);
		sonar = new EV3UltrasonicSensor(port);
	}

	/**
	 * 
	 * @return distance lue
	 */
	public float[] getRaw() {
		float[] sample = new float[1];
		sonar.fetchSample(sample, 0);
		return sample;
	}
}
