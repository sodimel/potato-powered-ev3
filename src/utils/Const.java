package utils;

import lejos.hardware.ev3.LocalEV3;

public class Const {
	
	public static String host = "192.168.1.33";
	
	
	public static final String LEFT_WHEEL = "D";
	public static final String RIGHT_WHEEL= "A";
	public static final String PINCES      = "B";
	public static final String COLOR_SENSOR      = "S2";
	public static final String TOUCH_SENSOR      = "S1";
	public static final String DISTANCE_SENSOR      = "S3";
	
	
	// Config pinces
 	public static final  int vitessePince = 2000;
 	public static final  int tempsOuverture = 500;
 	public static final int tempsFermeture = 500; 

}