package utils;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3TouchSensor;

/**
 * Extends the EV3TouchSensor to provide it with isPressed() functionality.
 */

public class TouchSensor 
{
	private Port port = null;
	private EV3TouchSensor touch = null;
	
    public TouchSensor()
    {
		port = LocalEV3.get().getPort(Const.TOUCH_SENSOR);
		touch= new EV3TouchSensor(port);
    }

	public boolean isPressed(){
		float[] sample = raw();
		return sample[0] != 0;
	}

    
	public float[] raw() {
		float[] sample = new float[1];
		touch.fetchSample(sample, 0);
		// TODO Auto-generated method stub
		return sample;
	}
	
}
