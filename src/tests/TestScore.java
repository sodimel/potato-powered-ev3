package tests;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import utils.Camera;
import utils.Score;

public class TestScore {
	
	private static Integer[][] palets;
	private static boolean run =true;
	public static void main(String[] args) {
		LCD.drawString("score actuel : 0", 0, 1);
		Camera vue = new Camera();
		Score score = new Score();
		while(run) {
			palets = vue.getPalets();
			//score.update(palets);
			LCD.clear();
			LCD.drawString("score actuel : "+score.getScore(), 0, 1);
			if(Button.RIGHT.isDown()){
				run = false;
			}
		}
		
	}
}
