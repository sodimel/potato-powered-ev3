package tests;

import lejos.hardware.Brick;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class TestObstacle {
	public void run(Brick ev3) {
        
		
		RegulatedMotor mA = new EV3LargeRegulatedMotor(MotorPort.A);
		RegulatedMotor mB = new EV3LargeRegulatedMotor(MotorPort.B);
		
		TextLCD lcd = ev3.getTextLCD();
		EV3UltrasonicSensor s3 = new EV3UltrasonicSensor(ev3.getPort("S3"));
		
		s3.enable();

		final SampleProvider sp = s3.getDistanceMode();
		
		int dist = getDistance(sp);
		
		do{
		lcd.drawString("Distance:", 0, 2);
		lcd.drawInt(dist, 3, 4); // display distance
		Delay.msDelay(500);
		lcd.clear(4);
		
		dist = getDistance(sp);
		
		}while(dist> 20);
		
		
		s3.disable();
		s3.close();
		lcd.clear();
	}
	
	private int getDistance(SampleProvider sp) {
		float [] dist = new float[sp.sampleSize()]; // our distance (in cm)
		sp.fetchSample(dist, 0); // get the current distance (0.1042)
		dist[0] = dist[0]*100; // get in cm (10.42 cm)
		return (int)dist[0]; // return int (10)
	}
}
