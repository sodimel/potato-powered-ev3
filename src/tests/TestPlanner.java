package tests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import Planner.CreateProblemFile;
import Planner.GenerateProblem;
import Planner.SolvePlan;
import Robot.Motion;
import fr.uga.pddl4j.util.BitOp;
import fr.uga.pddl4j.util.Plan;
import lejos.robotics.navigation.Waypoint;
import utils.PointPlanner;

public class TestPlanner {
	public static void main(String[] args) throws IOException {
		
		// list of palets' coordinate
		//palets (i,j) 0<=i<=2 0<=j<=4
		Integer[][] palets = {{1,1},{2,2},{2,4},{2,3}};
		
		File domain = new File("pddl/domain.pddl");
		File problem = new File("pddl/test.pddl");
		GenerateProblem c = new GenerateProblem(problem,palets);
		
		SolvePlan pddl = new SolvePlan(domain,problem);
		Plan p = pddl.getPlan();
		List<BitOp> actions = p.actions();
		for(BitOp bo : actions) {
			switch(bo.getName()) {
				case "move":				
					break;			
				case "pick-up":
					break;
				case "put-down":
					break;
				default:
					break;
			}
		}
		
	}
}
