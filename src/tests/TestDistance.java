package tests;

import lejos.hardware.Brick;
import lejos.hardware.Button;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

/**
 * 
 * @author co
 * TestDistance get values from UltraSonic Sensor on port S3 and display it on embedded screen.
 *
 */
public class TestDistance {
	public void run(Brick ev3) {
		

        TextLCD lcd = ev3.getTextLCD();
		EV3UltrasonicSensor s3 = new EV3UltrasonicSensor(ev3.getPort("S1"));
		
		s3.enable();

		final SampleProvider sp = s3.getDistanceMode();
		
		boolean done = false;
		while(!done) {

			int dist = getDistance(sp);
			lcd.drawString("Distance:", 0, 2);
			lcd.drawInt(dist, 3, 4); // display distance
			Delay.msDelay(500);
			lcd.clear(4);

			if (Button.readButtons() != 0)
	            done = true;
		}
		s3.disable();
		s3.close();	
	}
	
	private int getDistance(SampleProvider sp) {
		float [] dist = new float[sp.sampleSize()]; // our distance (in cm)
		sp.fetchSample(dist, 0); // get the current distance (0.1042)
		dist[0] = dist[0]*100; // get in cm (10.42 cm)
		return (int)dist[0]; // return int (10)
	}
}
