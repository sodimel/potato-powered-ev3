package motors;

import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.Move;
import lejos.robotics.navigation.MoveListener;
import lejos.robotics.navigation.MovePilot;
import lejos.robotics.navigation.MoveProvider;
import lejos.utility.Delay;
import utils.Const;

import java.util.Date;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;

public class Roues implements MoveListener {

    private EV3LargeRegulatedMotor rDroite;
    private EV3LargeRegulatedMotor rGauche;
    private MovePilot pilot;
 	private Wheel roueDroite;
 	private Wheel roueGauche;
 	private Chassis chassis;
 	private boolean isMoving;
    
    private static float maxVitesseRoue = 400;
    private float vitesseRoues;
    
    private long      before = -1;
    private double angle;
    

    public Roues(){
    	
    	this.rDroite = new EV3LargeRegulatedMotor(MotorPort.A);
    	this.rGauche = new EV3LargeRegulatedMotor(MotorPort.D);
    	this.vitesseRoues = maxVitesseRoue;
    	isMoving = false;
    	this.rDroite.setSpeed(vitesseRoues);
    	this.rGauche.setSpeed(vitesseRoues);
    	
    	this.roueGauche = WheeledChassis.modelWheel(this.rGauche, 56).offset(-1*62.525f);
    	this.roueDroite = WheeledChassis.modelWheel(this.rDroite, 56).offset(62.525f);
    	this.chassis = new WheeledChassis(new Wheel[]{roueGauche, roueDroite},  WheeledChassis.TYPE_DIFFERENTIAL);
    	this.pilot = new MovePilot(chassis);
    	this.pilot.setLinearSpeed(maxVitesseRoue);
    	this.pilot.setAngularSpeed(maxVitesseRoue);
		pilot.addMoveListener(this);
		this.angle = 0;
		
		
    }
    
    
    public boolean isMoving() {
    	return pilot.isMoving();
    }
	public void setVitesse(float v) {
		vitesseRoues = v;
		pilot.setLinearSpeed(v);
	}
	
	
	public void reculer() {
		pilot.backward();
	}

	
	public void avancer() {
		pilot.forward();
	}
	
	
	public void avancer2() {
		pilot.forward();
		Delay.msDelay(100); 
	}
	
	public void arreter() {
		pilot.stop();
	}
	
	public void moveto(double distance) {
		pilot.travel(distance,true);
	}

	public void rotate(double i) {
		pilot.rotate(i);
	}
	
	public void rotate(double i, boolean aGauche, double speed) {
		pilot.setAngularSpeed(speed);
		if(aGauche){
			pilot.rotate(i*-1);
			this.angle = angle + i ;
		}else{
			pilot.rotate(i);
			this.angle = angle - i;
		}
		while(angle >= 360){
			this.angle -= 360;
		}
		while(angle < 0){
			this.angle += 360;
		}
	}
	
	public void demiTour(){
		rotate(180, true, 120);
	}

	
	
	public void moveStarted(Move event, MoveProvider mp) {
		isMoving = true;	
	}

	
	public void moveStopped(Move event, MoveProvider mp) {
		isMoving = false;
	}

	public double getAngle(){
		return angle;
	}
	
	public void setAngle(double angle){
		this.angle = angle;
	}
	
	
	public void revenirAngleInitial(boolean face, float speed){
		if(face){
			if(angle > 180){
				rotate(360-angle, false, speed);
			}else{
				rotate(-angle, false, speed);
			}
		}else{
			rotate(180-angle, false, speed);
		}
		
	}
	
	
	public double angleInitial(boolean face){
		if(face){
			if(angle > 180){
				return 360-angle;
			}else{
				return -angle;
			}
		}else{
			return 180-angle;
		}
		

	}


}
