package motors;

import utils.Const;
import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.utility.Delay;

public class Pinces {

	private EV3LargeRegulatedMotor pince;
	private Port port;

	
	private boolean isClosed = false;
	private boolean isOpen  = true;
	private boolean isMoving = false;


	public Pinces(){
		port   = LocalEV3.get().getPort(Const.PINCES);
		pince = new EV3LargeRegulatedMotor(port);
		
	}


	// Calibration de l'ouverture et fermeture de la pince
	public void calibration(){
		this.pince.setSpeed(400);
		// Message de prevention
		System.out.println("Calibration de la pince");
		System.out.println("Pinces ouvertes ? (Oui : RIGHT/ Non : LEFT)");
		
		boolean reAsk = true;
		while(reAsk){
			Button.waitForAnyPress();
			if(Button.LEFT.isDown()){
				System.out.println("Appuyez sur OK");
				pince.forward(); // Ouvrir la pince
				Button.ENTER.waitForPressAndRelease();
				pince.stop();
				reAsk = false;
			}else if(Button.RIGHT.isDown()){
				reAsk = false;
			}else{
				System.out.println("Pinces ouvertes ? (Oui : RIGHT/ Non : LEFT)");
			}
		}
		
		// Fermeture de la pince
		System.out.println("Fermeture sur Palet");
		Button.ENTER.waitForPressAndRelease();
		System.out.println("Appuyez sur OK");
		pince.backward(); // Fermer la pince
		Button.ENTER.waitForPress();
		pince.stop();
		
		isOpen = false; // Ouverture a la fin de la calibration
		this.pince.setSpeed(Const.vitessePince);
		open();
	}
	
	// Ouverture de la pince
	public void open(){
		if(!isOpen){ // Uniquement si elle est fermee
			pince.forward();
			Delay.msDelay(Const.tempsOuverture); 
			pince.stop();
		}
		isOpen = true;
	}
	
	// Fermeture de la pince 
	public void close(){
		this.pince.setSpeed(Const.vitessePince);
		if(isOpen){ // Uniquement si elle est ouverte
			pince.backward();
			Delay.msDelay(Const.tempsFermeture); 
			pince.stop();
		}
		isOpen = false;
	}
}
