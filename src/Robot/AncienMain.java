package Robot;

import java.util.ArrayList;
import java.util.List;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.robotics.geometry.Point;
import lejos.robotics.navigation.Waypoint;
import lejos.utility.Delay;

public class AncienMain {

	public static Motion motion;
	public static Perception perception;
	private static Field field;
	
	public static void main(String[] args) {
		
		LCD.clear();
		LCD.drawString("Init Motor", 0, 1);
		Motion.initialize();
		LCD.clear();
		LCD.drawString("Motor init ok", 0, 1);
		LCD.drawString("press button..", 0, 3);
		Motion.buttons.waitForAnyPress();
		
		LCD.clear();
		LCD.drawString("Init Perception", 0, 1);
		Perception.initialize();
		LCD.clear();
		LCD.drawString("Perception init ok", 0, 1);
		LCD.drawString("press button..", 0, 3);
		Motion.buttons.waitForAnyPress();
		LCD.clear();
			
		field = new Field(new Point(0,0), 0);
		
		List<Waypoint> paletsToRetrieve = new ArrayList<Waypoint>();
	
		//Palets waypoints created for test purposes...
		paletsToRetrieve.add(new Waypoint(50 ,30)); // pallet 1
		paletsToRetrieve.add(new Waypoint(50 ,-20)); // pallet 2
		
		for (Waypoint wpt : paletsToRetrieve) {
			Motion.pickPalet(wpt);	
			//field.updatePos(wpt,0);
			LCD.clear();
			LCD.drawString(""+Motion.holding, 0, 1);
			Button.waitForAnyPress();
            if (Motion.holding) {
            	Motion.dropPalet(new Waypoint(Motion.poseP.getPose().getX(), 100));
            	//field.updatePos(enemy,0);
            }
	}
		Motion.closeAll();
	
	}
}
