package Robot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lejos.hardware.Battery;
import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.Keys;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.sensor.SensorMode;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.localization.OdometryPoseProvider;
import lejos.robotics.localization.PoseProvider;
import lejos.robotics.navigation.MovePilot;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.utility.Delay;
import utils.TouchSensor;

public class Motion {
	
	public static boolean isOpen = false;
	public static boolean holding = false;
	private static int OPEN = 540;
	private static int CLOSE = -540;
	
	//speed of wheels in cm persecond
	private static int LINEAR_SPEED = 15;
	//speed of claw in turns per second
	private static int CLAW_SPEED = 720;
	public static Brick ev3brick;
	private static Chassis chassis;
	public static PoseProvider poseP;	
	public static Keys buttons;
	public static MovePilot pilot;
	public static Navigator nav;
	
	
	public Motion(){
		
	}
	
	public static void initialize() {
		//Initialization
		ev3brick  = BrickFinder.getLocal();                     
        buttons = ev3brick.getKeys();
		Wheel wheel1 = WheeledChassis.modelWheel(Motor.A, 5.6).offset(-6.2525f);
		Wheel wheel2 = WheeledChassis.modelWheel(Motor.D, 5.6).offset(6.2525f);
		chassis = new WheeledChassis(new Wheel[]{wheel1, wheel2},WheeledChassis.TYPE_DIFFERENTIAL); 
		pilot = new MovePilot(chassis);
		poseP = new OdometryPoseProvider(pilot);
		nav = new Navigator(pilot ,poseP);
		//nav.setPoseProvider(chassis.getPoseProvider());
		//nav.getPoseProvider().setPose(chassis.getPoseProvider().getPose());
		//Motor.C.setSpeed(CLAW_SPEED);
		//pilot.setLinearSpeed(LINEAR_SPEED);
		
		if(!isOpen)
			openClaw();
		nav.singleStep(true);
		LCD.clear();
		LCD.drawString("Init complete..", 0, 1);
		Delay.msDelay(500);
		//pilot.setLinearSpeed(speed);
		//displayLocation();
		
		Waypoint origin = new Waypoint (0,30);
		poseP.setPose(new Pose(origin.x, origin.y, 0));
		
	}

	
	static void openClaw() {
			if(!isOpen) {				
				Motor.C.forward();
				Delay.msDelay(2000);
				Motor.C.stop();
				//Motor.C.rotateTo(OPEN);
				isOpen = true;
			}
		}
	
	static void closeClaw() {
			if(isOpen) {
				//Motor.C.rotateTo(CLOSE);
				Motor.C.backward();
				Delay.msDelay(2000);
				Motor.C.stop();
				isOpen = false;
			}
			
		}
	
	static int pickPalet(Waypoint wpt) {
		
		int success  = 1;				
		navigate(wpt);
   		Delay.msDelay(400);
   		
   		if(holding) {
   			LCD.clear ();
   			LCD.drawString ("is open: "+isOpen, 0, 1);
   			LCD.drawString ("vou fechar a garra", 0, 4);   			
   			closeClaw();
   			nav.rotateTo(45);
   			nav.waitForStop();
   			pilot.forward();
   			Delay.msDelay(300);
   			pilot.stop();
   			
   			}else { //FAILURE TO FIND PALET
   			LCD.clear ();
   			LCD.drawString ("failll held: nothing"+holding, 0, 1);   	
   			return 0;
   			
   		}
   		   						
		return success ;
		
	}
	
	static int dropPalet(Waypoint wpt) {
		
		int finish = 0;
		
		if(holding) {			
			navigate(wpt);
			openClaw();	   		
			turnAround();
			holding = false;
		}else {
			LCD.clear();
			LCD.drawString("ERROR", 0, 1);			
		}			
		finish = 1;
		//Motor.C.rotateTo(CLOSE); // seems unnecessary
		//printLocation();		
		return finish;
	}

	
	public static void navigate(Waypoint wpt) {	
		/*
		int angle = (int) poseP.getPose().angleTo(wpt);
		int distance = (int) poseP.getPose().distanceTo(wpt);
		pilot.rotate(angle);
		pilot.travel(distance);
		*/
		LCD.clear ();
		LCD.drawString ("Navigating..", 0, 3);
		move(wpt);

		
		
	}
	
	public static void move(Waypoint wpt) {

		float expectedHeading = poseP.getPose().relativeBearing(wpt);
		expectedHeading += poseP.getPose().getHeading();
		
		float error = 5;
		
		LCD.clear();
		LCD.drawString("Going: "+wpt.x+" "+wpt.y,0, 4);
		nav.goTo(wpt);
		
		while(nav.isMoving() || pilot.isMoving()) {
			if( Perception.touchSensor.isPressed() ) {
				
				holding = true;							
			}
			//Perception.scan(); // Ici on ferait le scan de l'environment
		}
		nav.waitForStop();
		//poseP.setPose(new Pose(wpt.x, wpt.y, expectedHeading));
		float currentX, currentY;
		currentX = poseP.getPose().getX();
		currentY = poseP.getPose().getY();
		if(Math.abs(currentX - wpt.x) > error || Math.abs(currentY - wpt.y) > error || Math.abs(poseP.getPose().getHeading() - expectedHeading) > error) {
			LCD.clear();
			LCD.drawString("ERROR GRANDE\nCorrigindo...", 0, 4);
			poseP.setPose(new Pose(wpt.x, wpt.y, expectedHeading));
			//Button.waitForAnyPress();
		}
		
		
		displayLocation();
		/*
		LCD.clear();
		LCD.drawString("wpt: "+ wpt.x+" , " +wpt.y, 0 , 1);
		LCD.drawString("pose: "+ poseP.getPose().getX()+" , " +poseP.getPose().getY(), 0 , 3);
		LCD.drawString("Press any button..", 0, 6);
		buttons.waitForAnyPress();
		*/
		
		//poseP.getPose().setLocation(wpt);
		//poseP.setPose(new Pose(wpt.x, wpt.y, poseP.getPose().getHeading()));
		
		
	}

	public static void displayLocation() {
		
		LCD.clear();
		LCD.drawString("Location:", 0, 2);
		LCD.drawString(poseP.getPose().getX()+ ", "+ poseP.getPose().getY(), 0, 4);
		LCD.drawString("Heading: "+poseP.getPose().getHeading(), 0, 5);
		//buttons.waitForAnyPress();
	}
	
	public static void waitUp() {
		while(nav.isMoving() || pilot.isMoving()) {
			
		}
	}
	
	public static void turnAround() {
		
		LCD.clear();
		LCD.drawString("vou virar", 0, 1);
		//Button.waitForAnyPress();
		nav.rotateTo(180);
		LCD.clear();
		LCD.drawString("virei", 0, 1);
		
		//Button.waitForAnyPress();
		nav.waitForStop();
		float x = poseP.getPose().getX();
		
		pilot.backward();
		Delay.msDelay(500); // go backward for a second, should be about 15 cm of return
		pilot.stop();
		waitUp();
		float erro = 5;		
		//nav.rotateTo(0);
		float xTeorico = (float) (x-15); //correction nem sempre eh necessaria
		if (Math.abs(xTeorico - poseP.getPose().getX()) > erro || Math.abs(180-poseP.getPose().getHeading())>5) {
			poseP.setPose(new Pose(xTeorico, poseP.getPose().getY(), 180));
		}
		
		
		
		
	}
	
	public void showRAM() {
		LCD.clear();
		LCD.drawString("Free RAM:", 0, 0);
		// displaying free memory on the LCD screen
		LCD.drawInt(( int ) Runtime. getRuntime ().freeMemory(), 0, 1);
	}
	
	public static void closeAll() {
		while(nav.isMoving()) {
			
		}
		if(isOpen)
			closeClaw();
		LCD.clear();		
		LCD.drawString("Close all..", 0, 3);		
		//Delay.msDelay(5000);
   		LCD.drawString("battery: "+Battery.getVoltage(), 0, 5);
   		buttons.waitForAnyPress();
	}
	


		
}
