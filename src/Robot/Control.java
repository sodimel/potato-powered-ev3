package Robot;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Keys;
import lejos.utility.Delay;
import motors.Pinces;
import motors.Roues;
import utils.Camera;
import utils.Const;
import utils.Point;
import utils.TouchSensor;
import utils.Vision;

public class Control {
	
	private Point position;
	private Point goal;
	private double angle;
	private Roues propulsion;
	private Vision vision;
	private Pinces pinces;
	public static Keys buttons;
	public static Brick ev3brick;
	private Thread  visionThread ;
	final TouchSensor touch = new TouchSensor();
	private Camera camera;
	
	public Control() {
		ev3brick  = BrickFinder.getLocal();                     
        buttons = ev3brick.getKeys();
		propulsion = new Roues();
		vision = new Vision();
		pinces = new Pinces();
		
		position = new Point(100,280); // position initiale
		goal = new Point(150,20); // position finale
		Point palet = new Point(150,280); // palet au hasard
		
		takeIt(palet);
		
		/*while(true) {
			propulsion.avancer();
			//System.out.println(vision.getRaw()[0]);
			if(touch.isPressed()) {
				propulsion.arreter();
				pinces.close();
			}
		}*/
		
		
		//pinces.close();
		
	}
	// ON FAIT LE DEPLACEMENT
	// UNE FOIS FAIT ON REMET LE ROBOT A L ANGLE 0
	// ENSUITE GO BASE ENNEMI AVEC LE MEME STYLE DE DEPLACEMENT
	// ON CALIBRE
	// ON RECOMMENCE
	
	public void takeIt(Point palet) {
		double rotation = getAngle(palet);
		if(rotation<0) {
			propulsion.rotate(rotation,false,120);
		}
		else {
			propulsion.rotate(rotation,true,120);
		}
		
		System.out.println(getDistance(palet)*10);
		System.out.println("vision distance"+vision.getRaw()[0]);
		System.out.println("rotation degres "+rotation);
		double realDistance = vision.getRaw()[0]*1000;
		propulsion.moveto(realDistance+(realDistance*0.02));
		while(true) {
			if (touch.isPressed()) {
				propulsion.arreter();
				pinces.close();
				Delay.msDelay(500);
				if(rotation<0) {
					propulsion.rotate(rotation,true,120);
				}
				else {
					propulsion.rotate(rotation,false,120);
				}
				position = palet;
				Delay.msDelay(500);
				goal();
				break;
			}
		
		}
		
		//propulsion.avancer();
		
		/*Thread t = new Thread(new Runnable() {
		    public void run() {
		    	while (true) {
					if (touch.isPressed()) {
						pinces.close();
						pinces.open();
						break;
					}
		    	}
		    	//System.vision.getRaw()[0]
		    }
		});
		t.start();
		*/
		
	}

	
	public void goal() {
		
		double distance = getDistance(goal);
		propulsion.moveto(distance*1000);
		while(propulsion.isMoving());
		pinces.open();
		Delay.msDelay(500);
		position = goal;
	}
	

	

	
	/* la rotation � faire pour se d�placer vers le palet */
	public float getAngle(Point palet) {
	    float angle = (float) Math.toDegrees(Math.atan2(palet.getCoord1() - position.getCoord1(),palet.getCoord2() - position.getCoord2()));
	    return (float) (angle-(angle*0.01));
	}
	
	public double getDistance(Point palet) {
		double distance = Math.hypot(palet.getCoord1() - position.getCoord1(),palet.getCoord2() - position.getCoord2());
		return distance;
	}
}
