package Robot;

import java.util.ArrayList;
import java.util.List;

import lejos.hardware.lcd.LCD;
import lejos.robotics.geometry.Point;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;

public class Field {

	public Point pos;
	public List<Waypoint> paletsToRetrieve;
	public int LENGTH = 200 ;
	public int WIDTH = 300 ;
	public Perception perception;
	public float currentHeading;

	public Field(Point initial, float heading ) {
		this.pos = initial;		
		this.currentHeading = heading;
	}
	
	public void addPalet(Waypoint wpt) {
		paletsToRetrieve.size();		
	}
	
	public void updatePos(Waypoint wpt, float Heading) {
//		this.pos.x = wpt.x;
//		this.pos.y = wpt.y;
//		float expectedHeading = Motion.poseP.getPose().relativeBearing(wpt);
//		expectedHeading += Motion.poseP.getPose().getHeading();
//		this.currentHeading = expectedHeading;
//		displayFieldPosition();
		
		pos.x = Motion.poseP.getPose().getX();
		pos.y = Motion.poseP.getPose().getY();
		currentHeading= Motion.poseP.getPose().getHeading();
	}
	
	public void updatePose() {
		Motion.poseP.setPose(new Pose(pos.x, pos.y, currentHeading));
	}
	
	public void displayFieldPosition() {
		LCD.clear();
		LCD.drawString("Field Location:", 0, 2);
		LCD.drawString(this.pos.x+ ", "+ this.pos.y, 0, 3);
		LCD.drawString("Heading: "+this.currentHeading, 0, 4);
		LCD.drawString("Press button..", 0, 5);
		Motion.buttons.waitForAnyPress();
	}
	
	public void Scan() {
		//Scan color
		//Scan distance
		//...
	}
	
}
