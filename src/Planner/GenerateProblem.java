package Planner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class GenerateProblem {
	private FileOutputStream fos;
	private Integer[][] palets;
	
	public GenerateProblem(File file,Integer[][] palets) throws IOException {
		fos = new FileOutputStream(file);
		this.palets = palets;
		writeProblem();
	}
	
	public void writeDomain(String domainName) throws IOException {
		fos.write(("\t(:domain "+domainName+")\n").getBytes());
	}
	
	public void writeObjects() throws IOException {
		fos.write("\t\t".getBytes());
		for(int i = 0; i< palets.length; i++) {
			fos.write(("o"+i+" ").getBytes());
		}
		fos.write("- object\n".getBytes());
	}
	
	/**
	 * ---------------------
	 * |P00|P01|P02|P03|P04|
	 * |P10|P11|P12|P13|P14| out
	 * |P20|P21|P22|P23|P24|
	 * ---------------------
	 * @throws IOException
	 */
	public void writePoints() throws IOException {
		fos.write(("\t\tp00 p01 p02 p03 p04\n" + 
				"\t\tp10 p11 p12 p13 p14\n" + 
				"\t\tp20 p21 p22 p23 p24\n" + 
				"\t\tout").getBytes());
		
		fos.write(" - point\n".getBytes());
	}
	
	public void writeObject() throws IOException {
		fos.write("\t(:objects\n".getBytes());
		writeObjects();
		writePoints();
		fos.write("\t)\n".getBytes());
	}
	
	public void writeInit() throws IOException {
		fos.write("\t(:init\n".getBytes());
		
		fos.write("\t\t(handempty)\n".getBytes());
		fos.write("\t\t(at-point p00)\n".getBytes());
		fos.write(("\t\t(close-to p00 p01) (close-to p00 p10)\n" + 
				"\t\t(close-to p01 p00) (close-to p01 p02) (close-to p01 p11)\n" + 
				"\t\t(close-to p02 p01) (close-to p02 p12) (close-to p02 p03)\n" + 
				"\t\t(close-to p03 p02) (close-to p03 p04) (close-to p03 p13)\n" + 
				"\t\t(close-to p04 p03) (close-to p04 p14)\n" + 
				"\t\t(close-to p10 p00) (close-to p10 p11) (close-to p10 p20)\n" + 
				"\t\t(close-to p11 p01) (close-to p11 p10) (close-to p11 p12) (close-to p11 p21)\n" + 
				"\t\t(close-to p12 p02) (close-to p12 p11) (close-to p12 p13) (close-to p12 p22)\n" + 
				"\t\t(close-to p13 p12) (close-to p13 p03) (close-to p13 p14) (close-to p13 p23)\n" + 
				"\t\t(close-to p14 p04) (close-to p14 p13) (close-to p14 p24)\n" + 
				"\t\t(close-to p20 p10) (close-to p20 p21)\n" + 
				"\t\t(close-to p21 p11) (close-to p21 p20) (close-to p21 p22)\n" + 
				"\t\t(close-to p22 p12) (close-to p22 p21) (close-to p22 p23)\n" + 
				"\t\t(close-to p23 p13) (close-to p23 p22) (close-to p23 p24)\n" + 
				"\t\t(close-to p24 p14) (close-to p24 p23)\n" + 
				"\t\t(close-to out p04) (close-to out p14) (close-to out p24)\n").getBytes());
		
		for(int i = 0; i< 3; i++) {
			for(int j = 0; j< 5; j++) {
				fos.write(("\t\t(close-to p"+i+j+" out)\n").getBytes());
			}
		}

		for(int i = 0; i< palets.length; i++) {
			//where are the palets 
			fos.write(("\t\t(at o"+i+" p"+palets[i][0]+palets[i][1]+")\n").getBytes());
		}
		
		fos.write("\t)\n".getBytes());
	}
	
	public void writeGoal() throws IOException {
		fos.write("\t(:goal\n".getBytes());
		fos.write("\t\t(and \n".getBytes());
		
		fos.write("\t\t\t(handempty)\n".getBytes());
		fos.write("\t\t\t(at-point out)\n".getBytes());
		for(int i = 0; i< palets.length; i++) {
			fos.write(("\t\t\t(at o"+i+" out)\n").getBytes());
		}
		
		fos.write("\t\t)\n".getBytes());
		fos.write("\t)\n".getBytes());
	}
	
	public void writeProblem() throws IOException {
		fos.write("(define (problem prob)\n".getBytes());
		writeDomain("test");
		writeObject();
		writeInit();
		writeGoal();
		fos.write(")\n".getBytes());
	}
}
