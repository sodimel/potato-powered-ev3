package Planner;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.Remote;
import java.util.List;

import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.parser.ErrorManager;
import fr.uga.pddl4j.planners.ProblemFactory;
import fr.uga.pddl4j.planners.statespace.hsp.HSP;
import fr.uga.pddl4j.util.BitOp;
import fr.uga.pddl4j.util.Plan;
import fr.uga.pddl4j.heuristics.relaxation.Heuristic.Type;

public class SolvePlan implements Serializable, Remote {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ProblemFactory factory;
	private CodedProblem pb;
	private HSP planner;
	private Plan plan;
	
	public SolvePlan(File domain,File problem){
		initParser(domain,problem);
		encodeProblem();
		searchSolution();
	}
	
	public SolvePlan() {
		File domain = new File("pddl/domaintest.pddl");
		File problem = new File("pddl/problemtest.pddl");
		initParser(domain,problem);
		encodeProblem();
		searchSolution();
	}
	
	public void initParser(File domain, File problem) {
		factory = ProblemFactory.getInstance();
		ErrorManager errorManager = null;
		try { 
		  errorManager = factory.parse(domain, problem);
		} catch (IOException e) {
		  System.out.println("Unexpected error when parsing the PDDL planning problem description.");
		  System.exit(0);
		}
		if (!errorManager.isEmpty()) {
		  errorManager.printAll();
		  System.exit(0);
		} else {
		  System.out.println("Parsing domain file and problem file done successfully");
		}
	}
	
	public void encodeProblem() {
		pb = factory.encode();
		System.out.println("Encoding problem done successfully (" 
    		    + pb.getOperators().size() + " ops, "
    		    + pb.getRelevantFacts().size() + " facts).");
		if (!pb.isSolvable()) {
			  System.out.println("Goal can be simplified to FALSE. No search will solve it.");
			  System.exit(0);
			}
	}
	
	public void searchSolution() {
		planner = new HSP(60000, fr.uga.pddl4j.heuristics.relaxation.Heuristic.Type.FAST_FORWARD, 1,false, 100);
		
		plan = planner.search(pb);
		if (plan != null) {
		    System.out.println("Found plan as follows:");
		    System.out.println(pb.toString(plan));
		} else {
		    System.out.println("No plan found.");
		}
	}
	
	public Plan getPlan() {
		return this.plan;
	}
}
