package Planner;

import java.net.*;

import utils.Camera;  
public class Server
{  
public static void main(String[] args) throws Exception {  
  DatagramSocket ds = new DatagramSocket();   
  Camera camera = new Camera();
  Integer[][] palets = camera.getPalets();
  Integer[] palet1 = palets[0];
  String x = palet1[0].toString();
  String y = palet1[1].toString();
  String concat = x+";"+y;
  InetAddress ip = InetAddress.getByName("192.168.1.12");  
   
  DatagramPacket dp = new DatagramPacket(concat.getBytes(), concat.length(), ip, 3000);  
  ds.send(dp);  
  ds.close();  
}  
}  