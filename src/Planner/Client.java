package Planner;

import java.net.*;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;  
public class Client{  
	
public static void main(String[] args) throws Exception {  
  DatagramSocket ds = new DatagramSocket(3000);  
  byte[] buf = new byte[1024];  
  DatagramPacket dp = new DatagramPacket(buf, 1024);  
  ds.receive(dp);  
  String str = new String(dp.getData(), 0, dp.getLength());  
  LCD.drawString(str, 0, 1);
  System.out.println(str);  
  Button.waitForAnyPress();
  ds.close();  
  
}  
}  