(define (domain test)
  (:requirements :strips :typing)
  (:types object point)
  (:predicates
    (at-point ?x - point)
    (at ?x - object ?y - point)
    (close-to ?x - point ?y - point)
    (pickwith ?x - object)
    (handempty)
  )
  (:action move
    :parameters (?x ?y - point)
    :precondition (and (at-point ?x) (close-to ?x ?y))
    :effect (and (at-point ?y) (not (at-point ?x)))
  )
  (:action pick-up
    :parameters (?x - object ?y - point)
    :precondition (and (handempty) (at ?x ?y) (at-point ?y))
    :effect (and (not (handempty)) (not(at ?x ?y)) (pickwith ?x))
  )
  (:action put-down
    :parameters (?x - object ?y - point)
    :precondition (and (at-point ?y) (pickwith ?x))
    :effect (and (handempty) (at ?x ?y) (not (pickwith ?x)))
  )

)
