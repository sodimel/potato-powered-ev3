(define (domain test)
  (:requirements :strips :typing)
  (:types object point)
  (:predicates
    (at-point ?x - point)
    (at ?x - object ?y - point)
    (close-to ?x - point ?y - point)
    (pointempty ?x - point)
    (handempty)
    (handfull)
  )
  (:action move
    :parameters (?x ?y - point)
    :precondition (and (at-point ?x) (close-to ?x ?y) (handempty))
    :effect (and (at-point ?y) (not (at-point ?x)))
  )
  (:action move-with-object
    :parameters (?x ?y - point)
    :precondition (and (at-point ?x) (close-to ?x ?y) (handfull) (pointempty ?y))
    :effect (and (at-point ?y) (not (at-point ?x)))
  )
  (:action pick-up
    :parameters (?x - object ?y - point)
    :precondition (and (handempty) (at ?x ?y) (at-point ?y))
    :effect (and (not (handempty)) (not(at ?x ?y)) (handfull) (pointempty ?y))
  )
  (:action put-down
    :parameters (?x - object ?y - point)
    :precondition (and (at-point ?y) (handfull))
    :effect (and (handempty) (at ?x ?y) (not (handfull)))
  )

)
