(define 
	(domain BG)
	(:requirements :strips :typing)
	(:types place)
	(:predicates 
		(isPlaying ?p - place)
		(notPlaying ?p - place)
		(succ ?p - place ?y - place)
		(roboton ?p - place)
		(done)
		(devantpalet)
		(full_hand)
		)
	(:action seekpalet
		:parameters (?robot - place ?p - place)
		:precondition (and (roboton ?robot) (isPlaying ?p) (succ ?robot ?p))
		:effect (and (not (notPlaying ?p)) (notPlaying ?p) (devantpalet)))
	(:action getpalet
		:parameters () 
		:precondition (devantpalet)
		:effect (full_hand))
	(:action getback
		:parameters ()
		:precondition (full_hand)
		:effect (done))
	(:action move 
		:parameters (?robot - place ?p - place)
		:precondition (and (roboton ?robot) (notPlaying ?p) (succ ?robot ?p))
		:effect (and (not (roboton ?robot)) (roboton ?p)))
)

